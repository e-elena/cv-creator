import React, { Component } from 'react'
import Header from './components/Header'
import WorkExperience from './components/WorkExperience'
import WhyMe from './components/WhyMe'
import Proud from './components/Proud'
import Skills from './components/Skills'

class App extends Component {
  constructor() {
    super()
    this.state = {
      font: 18,
      shown: false
    }

    this.handleChangeFont = this.handleChangeFont.bind(this)
    this.showSizes = this.showSizes.bind(this)
    this.handleSwitchToPdf = this.handleSwitchToPdf.bind(this)
  }
  
  handleChangeFont(size) {
    this.setState({ font: size })
  }

  showSizes() {
    this.setState({ shown: !this.state.shown})
  }

  handleSwitchToPdf() {
    const app = document.getElementsByClassName('App')[0]
    app.classList.contains('pdf-mode') ?
      app.classList.remove('pdf-mode')
      : app.classList.add('pdf-mode')
  }

  render() {
    return (
      <div
        
        className="App"
        style={{fontSize: `${this.state.font}px`}}
      >
        <div className="main-wrapper">
          <Header personName="Ivan Petrov" personProf="java developer" size={this.state.font}/>
          <div className="main-content">
            <div className="column-left">
              <WorkExperience size={this.state.font}/>
            </div>

            <div className="column-right">
              <WhyMe size={this.state.font} />
              <Proud size={this.state.font} />
              <Skills size={this.state.font} />
            </div>
          </div>

          {/* Edit Panel */}

          <div className="edit-panel">

            <span className="edit-panel-item item-size" onClick={this.showSizes}>
            </span>

            { this.state.shown ? 
              <div className="edit-panel-size">
                <span onClick={() => { this.handleChangeFont(10)}}>10</span>
                <span onClick={() => { this.handleChangeFont(12)}}>12</span>
                <span onClick={() => { this.handleChangeFont(14)}}>14</span>
                <span onClick={() => { this.handleChangeFont(16)}}>16</span>
                <span onClick={() => { this.handleChangeFont(18)}}>18</span>
                <span onClick={() => { this.handleChangeFont(20)}}>20</span>
                <span onClick={() => { this.handleChangeFont(24)}}>24</span>
              </div>
              : false
            }

          </div>
          {/* //Edit Panel */}

          <div className="pdf-mode-button" onClick={() => { this.handleSwitchToPdf()}}>
            <span className="pdf-mode-button-on">PDF</span>
            <span className="pdf-mode-button-off">Edit</span>
          </div>

        </div>
      </div>
    );
  }
}

export default App

import React, { Component } from 'react'

class PhotoBlock extends Component {
  render() {
    const { photo } = this.props
    return (
      <div className="photo-block" style={{background: `url(${photo}) no-repeat center center`}}>
        <div className="edit-button"></div>
      </div>
    )
  }
}

export default PhotoBlock
import React, { Component } from 'react'


const OneCompany = ({companyName, startDate, finishDate, technologies, size}) => {
  return (
      <section className="person-info-section" style={{ marginBottom: `${size * 3}px` }}>
        <h3 style={{ fontSize: `${size * 1.2}px`}}>{companyName}</h3>
        <div className="work-period" style={{ fontSize: `${size * 2}px`}}>
          {startDate} {finishDate ? '-' : false} {finishDate}
        </div>

        <ul>
          <li>Development of part of transportation system - online booking and check-in, transactions and accesses control</li>
          <li>Bug fixing</li>
        </ul>

        <div className="work-tech">
          <div>Technologies used:</div>
          {technologies}
        </div>
      </section>
  )
}

class WorkExperience extends Component {
    render() {
        const { size } = this.props
        return (
          <section className="person-info">
            <h2 className="person-info-title"  style={{ fontSize: `${size * 1.7}px`, marginBottom: `${size * 1.3}px`}}>Work Experience</h2>
            <OneCompany
              companyName="Nestle"
              startDate="2017"
              finishDate="2018"
              technologies="Java, Spring framework, JavaScript, React js" 
              size={size}
            />
            <OneCompany
              companyName="Yahooo"
              startDate="2016"
              finishDate="2017"
              technologies="JavaScript, React js" 
              size={size}
            />
            <OneCompany
              companyName="Yahooo"
              startDate="2016"
              technologies="JavaScript, React js"
              size={size}
            />
          </section>
        )
    }
}

export default WorkExperience
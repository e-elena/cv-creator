
import React, { Component } from 'react'

const whyMe = [
  { 
    id: 1, 
    text: 'I’m multitasking person with interests in different sides of development'
  },
  { 
    id: 2, 
    text: 'I worked with frontend and backend sides of different projects'
  },
  { 
    id: 3, 
    text: 'I like to study new technologies and make my projects more velocity and productive.'
  },
]

class WhyMe extends Component {

  constructor() {
    super()
    this.state ={
      whyme: whyMe,
      value: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleErase = this.handleErase.bind(this)
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  handleSubmit(e) {
    if (!this.state.value) {
      return;
    }

    const newParagraph = this.state.value
    const day = new Date();

    this.setState(() => ({
      whyme: this.state.whyme.concat(
        {
          id: `${day.getHours()}` + day.getMinutes() + day.getSeconds(), 
          text: newParagraph
        }
      ),
      value: ''
    }))
  }

  handleErase() {
    this.setState({ whyme: [], value: '' })
  }

  onRemove(id) {
    const updatedWhyMe = this.state.whyme.filter(oneP => oneP.id !== id)
    this.setState({ whyme: updatedWhyMe })
  }

  render() {
      const { size } = this.props
      return (
          <section className="person-info" style={{ marginBottom: `${size * 3}px` }}>
            <h2 className="person-info-title" style={{ fontSize: `${size * 1.7}px`, marginBottom: `${size * 1.3}px`}}>Why me</h2>
            <div className="person-info-section">
              <div style={{ marginBottom: this.state.whyme ? '24px' : '0' }}>
                <ul className="whyMe">
                  {this.state.whyme.map((oneItem)=> (
                    <li key={oneItem.id}>
                      <span
                        className="erase"
                        onClick={
                          () => {
                            this.onRemove(oneItem.id)
                          }
                        }
                      />
                      {oneItem.text}
                    </li>
                  ))}
                </ul>
              </div>
            </div>

            <div className="input-submit">
                <textarea
                  value={this.state.value}
                  type="text"
                  onChange={this.handleChange}
                  placeholder="Type to add point..."
                  style={{marginBottom: '20px'}}
                  className="not-printed"
                />

                <button type="submit" onClick={this.handleSubmit} className="button-submit  not-printed">Add point</button>
                <button type="button" onClick={this.handleErase} className="button-erase not-printed">Clear all</button>
              </div>

          </section>
      )
  }
}

export default WhyMe
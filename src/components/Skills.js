import React, { Component } from 'react'

const skillsList = [
  {id: 'Java', name: 'Java'},
  {id: 'Spring Framework', name:'Spring Framework'},
  {id: 'React Js', name: 'React Js'}
]

class Skills extends Component {

    constructor() {
      super()
      this.state ={
       skills: skillsList,
       value: ''
      }

      this.handleChange = this.handleChange.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
      this.handleErase = this.handleErase.bind(this)
    }

    handleChange(e) {
      this.setState({ value: e.target.value });
    }

    handleSubmit(e) {
      e.preventDefault();
      if (!this.state.value.length) {
        return;
      }
      const newSkill = this.state.value

      this.setState(prevState => ({
        skills: prevState.skills.concat({id: newSkill, name: newSkill}),
        value: ''
      }));
    }

    onRemove(id) {
      const updatedSkills = this.state.skills.filter(oneSkill => oneSkill.id !== id)
      this.setState({ skills: updatedSkills })
    }

    handleErase() {
      this.setState({ skills: [], value: ''})
    }

    render() {
        const { size } = this.props
        return (
            <section className="person-info">
              <h2
                className="person-info-title"
                style={{ fontSize: `${size * 1.7}px`, marginBottom: `${size * 1.3}px` }}
              >
                STRENGTHS
              </h2>
              <form onSubmit={this.handleSubmit}>
                <div className="person-info-section" style={{ marginBottom: `${size * 3}px` }}>
                  
                    {this.state.skills.map((oneSkill)=> (
                        <span className="one-skill" key={oneSkill.id}>
                          {oneSkill.name}
                          <span className="delete-item not-printed" 
                            onClick={
                              () => {
                                this.onRemove(oneSkill.id)
                              }
                            }
                          />
                        </span>
                      ))
                    }

                    <div className="input-submit">
                      <input
                        value={this.state.value}
                        type="text"
                        onChange={this.handleChange}
                        placeholder="Type to add skill..."
                        className="not-printed"
                        
                      />
                      <button type="submit" onClick={this.handleSubmit} className="button-submit not-printed">Add skill</button>
                      <button  type="button" onClick={this.handleErase} className="button-erase not-printed">Clear all</button>
                    </div>
                </div>
              </form>
            </section>
        )
    }
}

export default Skills
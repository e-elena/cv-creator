import React, { Component } from 'react'

const description = [
  {
    id: 1,
    text: 'I worked with frontend and backend sides of different projects'
  }
]

class Proud extends Component {

    constructor() {
      super()
      this.state ={
        description: description,
        value: ''
      }

      this.handleChange = this.handleChange.bind(this)
      this.handleSubmit = this.handleSubmit.bind(this)
      this.handleErase = this.handleErase.bind(this)
    }

    handleChange(e) {
      this.setState({ value: e.target.value });
    }

    handleSubmit(e) {
      if (!this.state.value) {
        return;
      }

      const newParagraph = this.state.value
      const day = new Date();

      this.setState(() => ({
        description: this.state.description.concat(
          {
            id: `${day.getHours()}` + day.getMinutes() + day.getSeconds(), 
            text: newParagraph
          }
        ),
        value: ''
      }))

    }

    handleErase() {
      this.setState({ description: [], value: '' })
    }

    onRemove(id) {
      const updatedDescription = this.state.description.filter(oneP => oneP.id !== id)
      this.setState({ description: updatedDescription })
    }

    render() {
        const { size } = this.props
        return (
            <section className="person-info" style={{ marginBottom: `${size * 3}px` }}>
              <h2
                className="person-info-title"
                style={{ fontSize: `${size * 1.7}px`, marginBottom: `${size * 1.3}px` }}
              >
                MOST PROUD OF MY LAST PROJECT
              </h2>
              <div className="person-info-section">
                <div style={{ marginBottom: this.state.description ? '20px' : '0' }}>
                  { this.state.description.length > 0 ? 
                      this.state.description.map(oneP =>(
                        <p key={oneP.id} className="proud-one-p">
                          <span
                            className="erase"
                            onClick={
                              () => {
                                this.onRemove(oneP.id)
                              }
                            }
                          />
                          {oneP.text}
                        </p>

                      ))
                    : false }
                </div>
              </div>

              <div className="input-submit">
                <textarea
                  value={this.state.value}
                  type="text"
                  onChange={this.handleChange}
                  placeholder="Type to add description..."
                  style={{marginBottom: '20px'}}
                  className="not-printed"
                />

                <button type="submit" onClick={this.handleSubmit} className="button-submit  not-printed">Add decription</button>
                <button type="button" onClick={this.handleErase} className="button-erase not-printed">Clear all</button>
              </div>
            </section>
        )
    }
}

export default Proud
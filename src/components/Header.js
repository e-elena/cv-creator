import React, { Component } from 'react'
import logo from './../assets/logo@2x.png'
import photo from './../assets/ava.jpg'
import PhotoBlock from './PhotoBlock'

class Header extends Component {
  constructor() {
    super()
    this.state ={
      name: 'Ivan Ivanov',
      prof: 'Java Developer',
      valueName: '',
      valueProf: '',
      editableMode: false
    }
    this.handleEdit = this.handleEdit.bind(this)
    this.handleChangeName = this.handleChangeName.bind(this)
    this.handleChangeProf = this.handleChangeProf.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
  }

  handleChangeName(e) {
    this.setState({ valueName: e.target.value });
  }
  handleChangeProf(e) {
    this.setState({ valueProf: e.target.value });
  }
  handleEdit() {
    this.setState(() => ({
      editableMode: !this.state.editableMode
    }))
  }
  handleSubmit() {
    if (!this.state.valueName && !this.state.valueProf ) {
      this.setState(() => ({
        editableMode: false
      }))
      return;
    }

    this.setState(() => ({
      name: this.state.valueName || this.state.name,
      prof: this.state.valueProf || this.state.prof,
      valueName: '',
      valueProf: '',
      editableMode: false
    }))
    
  }
  handleCancel() {
    this.setState(() => ({
      editableMode: false
    }))
  }

  render() {
      const { size } = this.props
      return (
          <header>
            <div className="column-left">
              <img src={logo} className="logo" alt="Smartbics" />

              { !this.state.editableMode ? 
                <div className="person">
                  <h1
                    className="person-name"
                    style={{ fontSize: `${size * 2.2}px` }}
                  >
                    {this.state.name}
                  </h1>
                  <h2
                    className="person-profession"
                    style={{ fontSize: `${size * 1.2}px` }}
                  >
                    {this.state.prof}
                  </h2>

                  <span className="edit-button not-printed" onClick={() => this.handleEdit()} />
                </div>
                :
                <form className="input-submit">
                  <input
                    value={this.state.valueName}
                    type="text"
                    onChange={this.handleChangeName}
                    placeholder="Type to change name..."
                    className="person-input not-printed"
                  />
                  <input
                    value={this.state.valueProf}
                    type="text"
                    onChange={this.handleChangeProf}
                    placeholder="Type to change profession..."
                    className="person-input not-printed"
                  />
                  <span onClick={this.handleSubmit} className="edit-button not-printed"/>
                  <span onClick={this.handleCancel} className="edit-button cancel not-printed"/>
                </form>
              }

            </div>
          
          <div className="column-right">
              <PhotoBlock photo={photo}/>
          </div>
          
        </header>
      )
  }
}

export default Header